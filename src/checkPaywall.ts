// check against list of urls to see if a site has a paywall.
// https://en.wikipedia.org/wiki/Category:Websites_utilizing_paywalls
// at some point this could instead be built using the wikipedia + wikidata apis,
// but that is really complicated

// list of sites that have a paywall, doesn't distinguish between hard and soft paywall.
const paywall_sites = [
	'adelaidenow.com.au',
	'adweek.com',
	'arabianbusiness.com',
	'theaustralian.com.au',
	'bloomberg.com',
	'bostonglobe.com',
	'bostonherald.com',
	'csmonitor.com',
	'couriermail.com.au',
	'telgraph.co.uk',
	'dailytelegraph.com.au',
	'thediplomat.com',
	'economist.com',
	'fijitimes.com.fj',
	'ft.com',
	'geelongadvertiser.com.au',
	'theglobeandmail.com',
	'heraldsun.com.au',
	'houstonchronicle.com',
	'chron.com',
	'independent.ie',
	'irishtimes.com',
	'latimes.com',
	'technologyreview.com',
	'nbr.co.nz',
	'nationalpost.com',
	'nytimes.com',
	'nnsl.com',
	'ntnews.com.au',
	'ocregister.com',
	'scientificamerican.com',
	'seattletimes.com',
	'techinasia.com',
	'theathletic.com',
	'thestar.com',
	'thetimes.co.uk',
	'vanityfair.com',
	'volkskrant.nl',
	'wsj.com',
	'washingtonpost.com',
	'wired.com'
];

// unused
// could use to distinguish if a site is a paywall
/*
const soft_paywalls = [

];
*/

export default function checkPaywall(url: string): boolean {
	for (let site of paywall_sites) {
		if (url.includes(site)) return true;
	}
	return false;
}