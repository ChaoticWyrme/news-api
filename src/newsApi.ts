import type {
	SearchOptions,
	SearchResult, SearchResultPaginated, SearchResultSinglePage,
	ErrorCode,
	ApiResponse, ApiSuccess, ApiFailure,
} from './types/api.types';
import type { Category } from './types/category.enum';
import type { Article } from './types/article.types';

const API_URL = 'https://newsapi.org/v2/top-headlines';

/**
 * A class for interacting with the news api.
 * 
 * Initialized with an apiKey and processes searches, based around the {@link Category} you want articles for.
 * Searches with a single Category are cached and can be paginated with {@link NewsApi#getPage}.
 * Note: available country codes are in /src/data/countries.json
 *
 * @class NewsApi
 */
export default class NewsApi {
	// can only be set
	private apiKey: string;
	// can be get or set
	private pageSize: number = 20;

	// cached request for pagination
	private lastRequest?: {
		category: Category,
		query: string,
		country?: string
	};

	constructor(apiKey: string) {
		this.apiKey = apiKey;
	}

	// Getters and setters
	public setApiKey(key: string) {
		this.apiKey = key;
	}

	public setPageSize(size: number) {
		this.pageSize = size;
	}

	public getPageSize() {
		return this.pageSize;
	}

	/**
	 * Builds the request from the parameters and sends it too the api, returning the response
	 *
	 * @param {Category} category The category to request.
	 * @param {string} query The query to filter the results with.
	 * @param {string} [country] The country to filter the results with.
	 * @param {number} [page] Pagination data.
	 * @returns {Promise<ApiResponse>} A promise of the Response.
	 * @throws {SearchError} if the api returns an error.
	 * @memberof NewsApi
	 */
	private async request(category: Category, query: string, country?: string, page?: number): Promise<ApiSuccess> {
		let url = this.createUrl({
			apiKey: this.apiKey,
			category,
			country,
			q: query,
			page,
			pageSize: this.pageSize,
		});

		let response: ApiResponse = await fetch(url.toString()).then(response => response.json());
		// ApiResponse is ApiFailure
		if (response.status === 'error') {
			throw new SearchError(response);
		}
		return <ApiSuccess>response;
	}

	private createUrl(options: SearchOptions): URL {
		let url = new URL(API_URL);

		let params = url.searchParams;

		let { apiKey, category, page, pageSize, country, q } = options;
		params.set('apiKey', apiKey);
		params.set('category', category as string);
		params.set('pageSize', pageSize.toString());
		params.set('q', q);

		if (page > 0) {
			params.set('page', options.page.toString());
		}
		if (country != null) {
			params.set('country', options.country);
		}

		return url;
	}

	/**
	 * Create a SearchResult from a single ApiResponse, and calculate pagination data from the response.
	 *
	 * @param {ApiSuccess} response The response to transform
	 * @param {number} [page=1] The page requested, for pagination data.
	 * @returns {SearchResult} The created result.
	 * @memberof NewsApi
	 */
	private createResult(response: ApiSuccess, page: number = 1): SearchResult {
		let { articles, totalResults } = response;

		if (page > 1 || totalResults > articles.length) {
			return <SearchResultPaginated>{
				articles,
				totalResults,
				paginate: true,
				page,
				pageCount: Math.ceil(totalResults / this.pageSize),
			};
		} else {
			return <SearchResultSinglePage>{
				articles,
				totalResults,
				paginate: false,
			};
		}
	}

	/**
	 * Combine several requests into one result, for when there is a request for multiple categories.
	 * Pagination data is specifically excluded, since pagination is only implemented for a single category request.
	 *
	 * @param {ApiSuccess[]} results The results to combine.
	 * @returns {SearchResultSinglePage} The result, with all of the components articles, but no pagination data.
	 * @memberof NewsApi
	 */
	private combineResponses(results: ApiSuccess[]): SearchResultSinglePage {
		let combined = results.reduce((acc, current) => {
			acc.articles.push(...current.articles);
			acc.totalResults += current.totalResults;
			return acc;
		});
		console.log(combined);
		console.log(this);
		combined.articles = this.sortArticles(combined.articles);
		// straight up lie about pagination because we can't do it properly with multiple results

		return {
			...combined,
			paginate: false
		};
	}

	/**
	 * Sorts the given articles by date
	 *
	 * @param {Article[]} articles The articles to sort.
	 * @returns {Article[]} The sorted array
	 * @memberof NewsApi
	 */
	private sortArticles(articles: Article[]): Article[] {
		return articles.sort((a, b) => {
			return (new Date(a.publishedAt).getTime()) - (new Date(b.publishedAt).getTime());
		});
	}

	/**
	 * Take in search parameters and call the api to return a result
	 *
	 * @param {string} query The query to search with
	 * @param {Category[]} categories The categories to get results from
	 * @param {string} [country] A country code to filter results
	 * @returns {Promise<SearchResult>} The result, with a list of articles, size, and pagination data
	 * @memberof NewsApi
	 */
	public search(query: string, categories: Category[], country?: string): Promise<SearchResult> {
		// single category search, can do pagination
		if (categories.length === 1) {
			return this.singleSearch(query, categories[0], country);
		} else {
			return this.multipleSearches(query, categories, country);
		}
	}

	/**
	 * Does a search with a single category, caching the results for pagination if there is enough results for it.
	 * @param query The string to search with.
	 * @param category The category to search with.
	 * @param country The country to filter the results to.
	 */
	async singleSearch(query: string, category: Category, country?: string): Promise<SearchResult> {
		let response = await this.request(category, query, country)
			.then(response => this.createResult(response));

		// cache responses for pagination
		if (response.paginate) {
			this.lastRequest = {
				category,
				query,
				country
			}
		}
		return response;
	}

	/**
	 * Does a search with multiple categories, requiring multiple requests.
	 * Because of this, it can't be paginated as easily, so that's not implemented.
	 * @param query Search query
	 * @param categories Search categories
	 * @param country Country to filter for
	 */
	async multipleSearches(query: string, categories: Category[], country?: string): Promise<SearchResultSinglePage> {
		let requests: Promise<ApiSuccess>[] = [];
		for (let cat of categories) {
			requests.push(this.request(cat, query, country));
		}
		// combine searches into one response
		// no pagination though
		let responses = await Promise.all(requests);
		return this.combineResponses(responses);

	}

	/**
	 * Gets the given page from the last query on the api
	 *
	 * @param {number} page the page to get from the api
	 * @returns {Promise<SearchResult>} The result from the api call
	 * @memberof NewsApi
	 */
	public getPage(page: number): Promise<SearchResult> {
		// null or undefined check
		if (this.lastRequest == null) {
			throw new Error("No cached result");
		}
		let request = this.lastRequest;
		return this.request(request.category, request.query, request.country, page)
			.then(response => this.createResult(response, page));
	}
}

/**
 * An error from the api, with a code and a message
 *
 * @export
 * @class SearchError
 * @extends {Error}
 */
export class SearchError extends Error {
	public code: ErrorCode;

	constructor(response: ApiFailure) {
		super(response.message)
		this.name = response.code;
		this.code = response.code;
	}
}