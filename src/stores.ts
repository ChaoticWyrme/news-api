import { writable } from 'svelte/store';

export let currentPage = writable(1);
export let settingsModal = writable(false);
export let aboutModal = writable(false);