// Type definitions for the news api.
import type { Category } from './category.enum';
import type { Article } from './article.types';

/**
 * Possible error codes from the api
 */
export type ErrorCode = ApiKeyErrors | ParameterErrors | RateLimitedError | SourceErrors | 'unexpectedError';

export type ApiKeyErrors = 'apiKeyDisabled' | 'apiKeyExhausted' | 'apiKeyInvalid' | 'apiKeyMissing';
export type ParameterErrors = 'parameterInvalid' | 'parametersMissing';
export type RateLimitedError = 'rateLimited';
export type SourceErrors = 'sourcesTooMany' | 'sourceDoesNotExist';

/**
 * What the api actually takes as values 
 *
 * @interface SearchOptions
 */
export type SearchOptions = {
	country?: string,
	category?: Category,
	sources?: string,
	q?: string,
	pageSize?: number,
	page?: number,
	apiKey: string
}

export type SearchResultSinglePage = {
	articles: Article[],
	totalResults: number,
	paginate: false
}

export type SearchResultPaginated = {
	articles: Article[],
	totalResults: number,
	paginate: true,
	page: number,
	pageCount: number
}

export type SearchResult = SearchResultSinglePage | SearchResultPaginated;

export type ApiSuccess = {
	status: 'ok',
	totalResults: number
	articles: Article[]
}

export type ApiFailure = {
	status: 'error',
	code: ErrorCode,
	message: string
}

export type ApiResponse = ApiSuccess | ApiFailure;