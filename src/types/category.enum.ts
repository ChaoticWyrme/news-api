/**
 * The available categories from the api.
 *
 * @export
 * @enum {number}
 */
export enum Category {
	Business = 'business',
	Entertainment = 'entertainment',
	General = 'general',
	Health = 'health',
	Science = 'science',
	Sports = 'sports',
	Technology = 'technology'
}