import type { Category } from './category.enum';

export type SearchDetails = {
	query: string,
	hidePaywallArticles: boolean,
	countryCode: string,
	categories: Category[]
}

export type SearchEvent = CustomEvent<SearchDetails>

export type SettingsDetails = {
	apiKey: string,
	resultsPerPage: number
}

export type SettingsEvent = CustomEvent<SettingsDetails>;