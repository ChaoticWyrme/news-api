// a list of all the modals in the app, for communication between the modal 
export enum Modal {
	Settings,
	About
};